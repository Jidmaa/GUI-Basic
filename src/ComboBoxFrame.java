import javax.swing.*;
import java.awt.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

/**
 * Created by majid on 5/14/17.
 */
public class ComboBoxFrame extends JFrame {
    private JComboBox imageJComboBox;
    private JLabel label;
    private String[] names = {"bug1.gif", "bug2.gif", "travelBug.gif", "buganim.gif"};
    private Icon[] icons = {
            new ImageIcon(getClass().getResource(names[0])),
            new ImageIcon(getClass().getResource(names[1])),
            new ImageIcon(getClass().getResource(names[2])),
            new ImageIcon(getClass().getResource(names[3]))};

    public ComboBoxFrame(){
        super("Testing JComboBox");
        setLayout(new FlowLayout());

        imageJComboBox = new JComboBox(names);
        imageJComboBox.setMaximumRowCount(3);
        imageJComboBox.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if (e.getStateChange() == ItemEvent.SELECTED){
                    label.setIcon(icons[imageJComboBox.getSelectedIndex()]);
                }
            }
        });

        add(imageJComboBox);

        label = new JLabel(icons[0]);
        add(label);
    }
}
