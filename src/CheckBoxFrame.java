import javax.swing.*;
import java.awt.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

/**
 * Created by majid on 5/14/17.
 */
public class CheckBoxFrame extends JFrame {
    private JTextField textField;
    private JCheckBox boldJCheckBox;
    private JCheckBox italicCheckBox;

    public CheckBoxFrame() {
        super("JCheckBox Test");
        setLayout(new FlowLayout());

        textField = new JTextField("Watch the font style change", 20);
        textField.setFont(new Font("Serif", Font.PLAIN, 14));
        add(textField);

        boldJCheckBox = new JCheckBox("Bold");
        italicCheckBox = new JCheckBox("Italic");
        add(boldJCheckBox);
        add(italicCheckBox);

        CheckBoxHandler checkBoxHandler = new CheckBoxHandler();
        boldJCheckBox.addItemListener(checkBoxHandler);
        italicCheckBox.addItemListener(checkBoxHandler);

    }

    private class CheckBoxHandler implements ItemListener {
        private int valBold = Font.PLAIN;
        private int valItalic = Font.PLAIN;

        @Override
        public void itemStateChanged(ItemEvent e) {
            if (e.getSource() == boldJCheckBox) {
                valBold = boldJCheckBox.isSelected() ? Font.BOLD : Font.PLAIN;
            }
            if (e.getSource() == italicCheckBox) {
                valItalic = italicCheckBox.isSelected() ? Font.ITALIC : Font.PLAIN;
            }
            textField.setFont(new Font("Serif", valBold + valItalic, 14));
        }
    }

}
