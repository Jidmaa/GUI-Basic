import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by majid on 5/14/17.
 */
public class ButtonFrame extends JFrame {
    private JButton plainButton;
    private JButton fancyButton;

    public ButtonFrame(){
        super("Testing Button");
        setLayout(new FlowLayout());

        plainButton = new JButton("Plain Button");
        add(plainButton);

        Icon bug1 = new ImageIcon(getClass().getResource("bug1.gif"));
        Icon bug2 = new ImageIcon(getClass().getResource("bug2.jpg"));

        fancyButton = new JButton("Fancy Button" , bug1);
        fancyButton.setRolloverIcon(bug2);
        add(fancyButton);

        ButtonHandler handler = new ButtonHandler();
        fancyButton.addActionListener(handler);
        plainButton.addActionListener(handler);

    }

    private class ButtonHandler implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            JOptionPane.showMessageDialog(ButtonFrame.this,String.format("You Pressed: %s" ,e.getActionCommand()));
        }
    }
}
