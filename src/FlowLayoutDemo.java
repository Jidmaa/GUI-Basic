import javax.swing.*;

/**
 * Created by majid on 5/15/17.
 */
public class FlowLayoutDemo {
    public static void main(String[] args) {
        FlowLayoutFrame flowLayoutFrame = new FlowLayoutFrame();
        flowLayoutFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        flowLayoutFrame.setSize(300 , 75);
        flowLayoutFrame.setVisible(true);
    }
}
