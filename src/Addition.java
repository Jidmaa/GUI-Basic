import javax.swing.*;

/**
 * Created by majid on 5/14/17.
 */
public class Addition {
    public static void main(String[] args) {
        String firstNum = JOptionPane.showInputDialog("Enter first integer");
        String secondNum = JOptionPane.showInputDialog("Enter second integer");
        int number1 = Integer.parseInt(firstNum);
        int number2 = Integer.parseInt(secondNum);
        int sum = number1 + number2;
        JOptionPane.showMessageDialog(null, "the sum is:" + sum, "Sum of two integers", JOptionPane.PLAIN_MESSAGE);
    }
}
