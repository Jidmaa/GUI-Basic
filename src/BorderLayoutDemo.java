import javax.swing.*;

/**
 * Created by majid on 5/15/17.
 */
public class BorderLayoutDemo {
    public static void main(String[] args) {
        BorderLayoutFrame borderLayoutFrame = new BorderLayoutFrame();
        borderLayoutFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        borderLayoutFrame.setSize(300,200);
        borderLayoutFrame.setVisible(true);
    }

}
