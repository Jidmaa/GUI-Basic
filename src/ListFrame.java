import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;

/**
 * Created by majid on 5/14/17.
 */
public class ListFrame extends JFrame {
    private JList colorJList;
    private final String[] colornames = {"Black", "Blue", "Cyan", "Dark Gray", "Gray",
            "Green", "Light Gray", "Magenta", "Orange", "Pink", "Red", "White", "Yellow"};
    private final Color[] colors = {Color.black, Color.blue, Color.CYAN, Color.DARK_GRAY, Color.GRAY,
            Color.GREEN, Color.LIGHT_GRAY, Color.MAGENTA, Color.ORANGE, Color.PINK, Color.RED, Color.WHITE, Color.YELLOW};

    public ListFrame(){
        super("List Test");
        setLayout(new FlowLayout());

        colorJList = new JList(colornames);
        colorJList.setVisibleRowCount(5);

        colorJList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        add(new JScrollPane(colorJList));

        colorJList.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                getContentPane().setBackground(colors[colorJList.getSelectedIndex()]);
            }
        });

    }
}
