import javax.swing.*;

/**
 * Created by majid on 5/16/17.
 */
public class JMenuDemo {
    public static void main(String[] args) {
        JMenuFrame jMenuFrame = new JMenuFrame();
        jMenuFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jMenuFrame.setSize(300,500);
        jMenuFrame.setVisible(true);
    }
}
