import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * Created by majid on 5/15/17.
 */
public class KeyDemoFrame extends JFrame implements KeyListener {
    private String line1;
    private String line2;
    private String line3;
    private JTextArea textArea;

    public KeyDemoFrame() {
        super("Demonstrating Keystroke Events");

        textArea = new JTextArea(10, 15);
        textArea.setText("Press any key on the keyboard...");
        textArea.setEnabled(false);
        textArea.setDisabledTextColor(Color.BLACK);
        add(textArea);

        addKeyListener(this);


    }

    @Override
    public void keyTyped(KeyEvent e) {
        line1 = String.format("Key typed: %s", e.getKeyText(e.getKeyCode()));
        setLine2and3(e);
    }

    @Override
    public void keyPressed(KeyEvent e) {
        line1 = String.format("Key pressed: %s", e.getKeyText(e.getKeyCode()));
        setLine2and3(e);
    }

    @Override
    public void keyReleased(KeyEvent e) {
        line1 = String.format("Key released: %s", e.getKeyText(e.getKeyCode()));
        setLine2and3(e);
    }

    private void setLine2and3(KeyEvent event) {
        line2 = String.format("this key is %san action key", event.isActionKey() ? "" : "Not ");
        String temp = event.getKeyModifiersText(event.getModifiers());

        line3 = String.format("Modifier keys pressed:%s", temp.equals("") ? "none" : temp);
        textArea.setText(String.format("%s\n%s\n%s\n", line1, line2, line3));
    }
}
