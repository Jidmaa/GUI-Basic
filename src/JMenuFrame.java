import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 * Created by majid on 5/15/17.
 */
public class JMenuFrame extends JFrame{
    private JMenuBar menuBar;
    private JMenu menu;
    private JMenu subMenu;
    private JMenuItem menuItem;
    private JRadioButtonMenuItem rbMenuItem;
    private JCheckBoxMenuItem cbMenuItem;

    public JMenuFrame(){
        super("Menu");

        setLayout(new FlowLayout());

        menuBar = new JMenuBar();

        menu = new JMenu("A Menu");
        menu.setMnemonic(KeyEvent.VK_A);
        menu.getAccessibleContext().setAccessibleDescription("the only menu in this program that has menu items");
        menuBar.add(menu);

        //menuItem = new JMenuItem("Both text and icon",new ImageIcon(getClass().getResource("bug1.gif")));
        menuItem = new JMenuItem("hoho");
        menuItem.setMnemonic(KeyEvent.VK_B);
        menu.add(menuItem);

        //menuItem = new JMenuItem(new ImageIcon(getClass().getResource("bug1.gif")));
        menuItem = new JMenuItem("haha");
        menu.setMnemonic(KeyEvent.VK_D);
        menu.add(menuItem);

        menu.addSeparator();

        ButtonGroup group = new ButtonGroup();

        rbMenuItem = new JRadioButtonMenuItem("A radio button menu item");
        rbMenuItem.setSelected(true);
        rbMenuItem.setMnemonic(KeyEvent.VK_R);
        group.add(rbMenuItem);
        menu.add(rbMenuItem);

        rbMenuItem = new JRadioButtonMenuItem("Another one");
        rbMenuItem.setMnemonic(KeyEvent.VK_O);
        group.add(rbMenuItem);
        menu.add(rbMenuItem);

        menu.addSeparator();
        cbMenuItem = new JCheckBoxMenuItem("A checkbox menu item");
        cbMenuItem.setMnemonic(KeyEvent.VK_C);
        menu.add(cbMenuItem);

        cbMenuItem = new JCheckBoxMenuItem("Another one");
        cbMenuItem.setMnemonic(KeyEvent.VK_H);
        menu.add(cbMenuItem);

        menu.addSeparator();
        subMenu = new JMenu("A SubMenu");
        subMenu.setMnemonic(KeyEvent.VK_S);

        menuItem = new JMenuItem("An item in the subMenu");
        menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_2, ActionEvent.ALT_MASK));
        subMenu.add(menuItem);

        menuItem = new JMenuItem("Another item");
        subMenu.add(menuItem);
        menu.add(subMenu);

        menu = new JMenu("Another menu");
        menu.setMnemonic(KeyEvent.VK_N);
        menu.getAccessibleContext().setAccessibleDescription("this menu does nothing");
        menuBar.add(menu);

        add(menuBar);

        MenuHandler handler = new MenuHandler();
        menuItem.addActionListener(handler);
        rbMenuItem.addActionListener(handler);
        cbMenuItem.addActionListener(handler);

    }

    private class MenuHandler implements ActionListener,ItemListener{


        @Override
        public void actionPerformed(ActionEvent e) {
            System.exit(0);
        }

        @Override
        public void itemStateChanged(ItemEvent e) {
            System.out.println("kir");
        }
    }

}
