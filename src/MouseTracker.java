import javax.swing.*;

/**
 * Created by majid on 5/15/17.
 */
public class MouseTracker {
    public static void main(String[] args) {
        MouseTrackerFrame mouseTrackerFrame = new MouseTrackerFrame();
        mouseTrackerFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mouseTrackerFrame.setSize(300,100);
        mouseTrackerFrame.setVisible(true);
    }
}
